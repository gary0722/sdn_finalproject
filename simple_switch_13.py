# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib import dpid as dpid_lib
from ryu.lib import stplib
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import tcp
from ryu.lib.packet import arp
from ryu.app import simple_switch_13
import ryu.app.ofctl.api as ofctl_api


ARP_REQUEST = 1
ARP_REPLY = 2
AUTH_IP = '10.0.0.87'
AUTH_SERVER = '192.168.56.101'
LOGIN_HOST = '192.168.57.101'
TAFFIC = 100

class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.ip_to_mac = {}
        self.ip_to_port = {}
        self.user_to_ip = {}
        self.online_host = {}
        self.user_hosts = {}
        self.feature = 0
        self.meterExist = {}
        self.online_host[AUTH_SERVER] = -1
        self.online_host[LOGIN_HOST] = -1

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    def switch_features(self,datapath):	
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        print('switch_features')
		#all drop
        match = parser.OFPMatch()
        actions = []
        self.add_flow(datapath, 1, match, actions)
		
		#for login server to LOGIN_HOST
        match1 = parser.OFPMatch(eth_type=0x0800,ipv4_src=AUTH_SERVER,ipv4_dst=LOGIN_HOST)
        actions1 = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 2, match1, actions1)

        match2 = parser.OFPMatch(eth_type=0x0800,ipv4_src=LOGIN_HOST,ipv4_dst=AUTH_SERVER)
        actions2 = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 2, match2, actions2)

		#for login message
        match3 = parser.OFPMatch(eth_type=0x0800,ipv4_dst=AUTH_IP)
        actions3 = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 2, match3, actions3)

		#for arp
        match = parser.OFPMatch(eth_type=0x0806)
        actions4 = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 2, match, actions4)	

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        
        #if ev.msg.msg_len < ev.msg.total_len:
        #    self.logger.debug("packet truncated: only %s of %s bytes",
        #                      ev.msg.msg_len, ev.msg.total_len)
        print('packet in!!')
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
        ip = pkt.get_protocols(ipv4.ipv4)
        tcppkt = pkt.get_protocols(tcp.tcp)
        arppkt = pkt.get_protocols(arp.arp)
        eth_dst = eth.dst
        eth_src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})
        self.ip_to_port.setdefault(dpid, {})
        
        #print "get packet eth_dst is %s" %(eth_dst)
        if eth_dst == '01:80:C2:00:00:00':
			print('BPDU')
			print self.feature
        elif self.feature == 0:
			print ('feature')
			self.feature = 1
			allDatapath = ofctl_api.get_datapath(self)
			for i in allDatapath:
				self.switch_features(i)

        if arppkt:
            #print('arp')
            src_mac = arppkt[0].src_mac
            dst_mac = arppkt[0].dst_mac
            src_ip = arppkt[0].src_ip
            dst_ip = arppkt[0].dst_ip
            if dst_ip in self.online_host and src_ip in self.online_host:
                self.mac_to_port[dpid][src_mac] = in_port
                self.ip_to_port[dpid][src_ip] = in_port

                if dst_ip in self.ip_to_port[dpid]:
                    out_port = self.ip_to_port[dpid][dst_ip]
                else:
                    out_port = ofproto.OFPP_FLOOD
                actions = [parser.OFPActionOutput(out_port)]

                if out_port != ofproto.OFPP_FLOOD:
					print('add arp flow')
					if src_ip in self.online_host:
						match = parser.OFPMatch(in_port=in_port, eth_type=0x0806,arp_tpa=dst_ip)
						self.add_flow(datapath, 3, match, actions)

                data = None
                if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                    data = msg.data

                out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                          in_port=in_port, actions=actions, data=data)
                datapath.send_msg(out)
                return

        elif ip:
            #print('Get IP packet.')
            dst_ip = ip[0].dst
            src_ip = ip[0].src
            if tcppkt and dst_ip == AUTH_IP:
				#AUTH_Packet
				src_port = tcppkt[0].src_port
				dst_port = tcppkt[0].dst_port
				print('Get AUTH_Packet')
				print(dst_port, src_ip, dst_ip)
				self.online_host[src_ip] = tcppkt[0].dst_port
				self.user_hosts.setdefault(dst_port, {})
				self.user_hosts[dst_port][src_ip] = True
				
				#to all data path this ip packet in
				allDatapath = ofctl_api.get_datapath(self)
				matchA = parser.OFPMatch(eth_type=0x0800,ipv4_src=src_ip)
				actionsA = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,ofproto.OFPCML_NO_BUFFER)]
				for i in allDatapath:
					self.add_flow(i, 2, matchA, actionsA)

				matchA = parser.OFPMatch(eth_type=0x0800,ipv4_dst=src_ip)
				for i in allDatapath:
					self.add_flow(i, 2, matchA, actionsA)

				print "AUTH IP = %s , MemberID = %d" %(src_ip , dst_port)
				print "Add rule to all switch : ipv4_src=%s ,packet in" %(src_ip)
				print ''
				return
            if dst_ip in self.online_host and src_ip in self.online_host:
		print('')		
            else:
		return

            print "Packet In : Source IP = %s Dst IP = %s" %(src_ip,dst_ip)
            self.mac_to_port[dpid][eth_src] = in_port
            self.ip_to_port[dpid][src_ip] = in_port

            if dst_ip in self.ip_to_port[dpid]:
				out_port = self.ip_to_port[dpid][dst_ip]
            else:
				out_port = ofproto.OFPP_FLOOD
				print('flood')
            actions = [parser.OFPActionOutput(out_port)]

            if out_port != ofproto.OFPP_FLOOD:
				if dst_ip in self.online_host or src_ip in self.online_host:
					#meter
					s = 0
					userHost = {}
					for x in self.online_host:
						userHost[self.online_host[x]] = 0
					for x in self.online_host:
						userHost[self.online_host[x]]+=1
						if userHost[self.online_host[x]] == 1:
							s+=1

					if userHost[self.online_host[dst_ip]] > userHost[self.online_host[src_ip]]:
						#speed = TAFFIC/s/userHost[self.online_host[dst_ip]]
						meter_id_for_rule = self.online_host[dst_ip]
					else:
						#speed = TAFFIC/s/userHost[self.online_host[src_ip]]
						meter_id_for_rule = self.online_host[src_ip]
					if meter_id_for_rule == -1:
						meter_id_for_rule = 100
					print "online people = %d" %(s)

					for userid in userHost:
						meter_id = userid
						if meter_id == -1:
							meter_id = 100
						print "User %d has %d machine " %(meter_id,userHost[userid])
						speed = TAFFIC/s/userHost[userid]
						print "\tMeter speed : 100000 /online people / member's online machine, %d " %(speed)
						print "\tadd meter id = %d speed = %d" %(meter_id,speed)
						bands = [parser.OFPMeterBandDrop(type_=ofproto.OFPMBT_DROP, len_=16,
										rate=speed, burst_size=1000)]
						if meter_id in self.meterExist:	
							req=parser.OFPMeterMod(datapath=datapath, command=ofproto.OFPMC_MODIFY,
									flags=ofproto.OFPMF_KBPS,meter_id=meter_id, bands=bands)
						else:
							req=parser.OFPMeterMod(datapath=datapath, command=ofproto.OFPMC_ADD,
									flags=ofproto.OFPMF_KBPS,meter_id=meter_id, bands=bands)
						datapath.send_msg(req)
						self.meterExist[meter_id] = 1
						print ''

					match = parser.OFPMatch(in_port=in_port,eth_type=0x0800,ipv4_dst=dst_ip)
						
					inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions),
								parser.OFPInstructionMeter(meter_id_for_rule,ofproto.OFPIT_METER)]
					mod = datapath.ofproto_parser.OFPFlowMod(datapath=datapath, match=match, cookie=0,command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0, priority=5, instructions=inst)
					datapath.send_msg(mod)
					print "xid %d Add New Rule,Match in_port=%d ipv4_dst=%s , meter_id = %d , output_port = %s" %(dpid,in_port,dst_ip,meter_id_for_rule,out_port)
			
            data = None
            if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                data = msg.data

            out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                      in_port=in_port, actions=actions, data=data)
            datapath.send_msg(out)
