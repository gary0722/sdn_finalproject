void server_init(char *port){

	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	serverPort = atoi(port);

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	fdmax = listenfd;
	if(listenfd < 0){
		perror("socket");
		exit(0);
	}

	/* server info */
	bzero(&serverInfo, sizeof(serverInfo));
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = htonl(INADDR_ANY);
	serverInfo.sin_port = htons(serverPort);

	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &serverInfo, sizeof(serverInfo));
	if(bind(listenfd,(struct sockaddr *) &serverInfo, sizeof(serverInfo)) < 0 ){
		perror("bind");
		exit(0);
	}

    if(listen(listenfd, 1024) < 0){
		perror("listen");
		exit(0);
	}

	FD_SET(listenfd, &master);	

	userData = (struct UserData *)malloc(userMAX * sizeof(struct UserData));

	int i;
	for(i = 0 ; i < userMAX ; i++){
		memset(&userData[i],0,sizeof userData[i]);
		userData[i].socket = -1;
		userData[i].id = i;
		userData[i].step = 0;
	}
}
void newClient(){
	int addrlen = sizeof(clientInfo);
	newfd = accept(listenfd,(struct sockaddr*) &clientInfo, &addrlen);
	if (newfd == -1) {
		perror("accept");
	}else{
		//for new user
		FD_SET(newfd, &master);
		if(newfd > fdmax){
			fdmax = newfd;
		}
		int newIndex = getUserDataMemory(newfd);
		struct in_addr ipAddr = clientInfo.sin_addr;
		inet_ntop(AF_INET, &ipAddr, userData[newIndex].addr, INET_ADDRSTRLEN );
		sprintf(userData[newIndex].port, "%d",clientInfo.sin_port);	

		strcpy(socketOutputBuffer,"****************************************\n");
		strcat(socketOutputBuffer,"** Welcome to the login server. **\n");
		strcat(socketOutputBuffer,"****************************************\n");
		strcat(socketOutputBuffer,"Please input your Account:");
		write(newfd, socketOutputBuffer, strlen(socketOutputBuffer));
	}	
}
void oldClient(int fd){
	char buf[lineMAX];
	memset(buf,0,sizeof buf);
	int nbytes = read(fd,buf,lineMAX);	
	int index = getUserDataIndexBySocket(fd);
	
	if (nbytes != 0) {
		buf[strcspn(buf, "\n")] = '\0';
	    buf[strcspn(buf, "\r")] = '\0';
		if(userData[index].step == 0){
			strcpy(userData[index].account,buf);
			strcpy(socketOutputBuffer, "Please input your Password:");
			userData[index].step = 1;
		}else{
			strcpy(userData[index].password,buf);
			userData[index].step = 0;
			strcpy(socketOutputBuffer, "Your are :\n");
			strcat(socketOutputBuffer, userData[index].account);
			strcat(socketOutputBuffer, "\n");
			strcat(socketOutputBuffer, userData[index].password);
			if(authentication(userData[index].account, userData[index].password)){
				strcat(socketOutputBuffer, "\nAuthentication success.\n");
			}else{
				strcat(socketOutputBuffer, "\nAuthentication fail\n");
			}
			write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
			closeUser(fd);
		}
		write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
	}
}

int authentication(char *account,char *password){
	int result = 0;
	FILE *fp;
	fp = fopen(AC, "r");
	if(fp == NULL){
		printf("Error: fopen\n");
		exit(0);
	}
	
    char buf[50];
	char data[2][50];

    while(fgets(buf,49,fp) != NULL){
		buf[strcspn(buf, "\n")] = '\0';
		strcpy(data[0],buf);
		fgets(buf,512,fp);
		buf[strcspn(buf, "\n")] = '\0';
		strcpy(data[1],buf);
		if((strcmp(data[0],account)) == 0 && (strcmp(data[1],password) == 0)){
			result = 1;
		}
    }
    fclose(fp);

	return result;
}
