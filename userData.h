struct UserData{
    int id;
    char port[6];
    int socket;
    char addr[INET_ADDRSTRLEN];
    int step; //0 for Account,1 for Password
    char account[10];
    char password[10];
	int ipListCount;
};
struct UserData *userData;

int getUserDataMemory(int);
int getUserDataIndexBySocket(int);
void closeUser(int,fd_set *);

