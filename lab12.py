from mininet.topo import *
from mininet.net import *
from mininet.link import *
from mininet.util import *
from mininet.log import *
from mininet.node import *
class MyTopo( Topo ):
	"Simple topology example."

	def __init__( self ):
		"Create custom topo."

		# Initialize topology
		Topo.__init__( self )

		# Add hosts and switches
		core = []
		edgeSwitch = []
		aggSwitch = []
		host = []
		
		for i in range(1,5):
			switch = self.addSwitch( '300%s' %i )
			edgeSwitch.append(switch)
			switch = self.addSwitch( '200%s' %i )
			aggSwitch.append(switch)
		
		for i in range(1,3):
			switch = self.addSwitch( '100%s' %i )
			core.append(switch)
		for i in range(1,9):
			h = self.addHost('h%s' %i)
			host.append(h)

        # Add links
		for i in range(0,2):
			index = 0 if i < 2 else 1
			for j in range(0,2):
				self.addLink(core[i],aggSwitch[index+j*2],bw=1000)	
		
		for i in range(0,4):
			index = i+1 if i%2 == 0 else i-1
			self.addLink(aggSwitch[i],edgeSwitch[i],bw=100)
			self.addLink(aggSwitch[i],edgeSwitch[index],bw=100)
		
		for i in range(0,8):
			index = i/2
			self.addLink(host[i],edgeSwitch[index],bw=100)

def myTest():
	topo = MyTopo()
	net = Mininet(topo=topo, link=TCLink,controller=None)
	net.addController('myController',controller=RemoteController,ip='127.0.0.1')
	net.start()
	dumpNodeConnections(net.hosts)
	CLI(net)
	net.stop()

if __name__ == '__main__':
	setLogLevel('info')
	myTest()
#topos = { 'mytopo': ( lambda: MyTopo() ) }
