#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <arpa/inet.h>
#include<unistd.h>
#include<errno.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define userMAX 10
#define lineMAX 100
#define AC "AC"
#define idtoip "idtoip"

#include "userData.h"
#include "userData.c"

void server_init(char*);
void newClient();
void oldClient(int);
int authentication(char*,char*,int);

/* Global Variables */
int serverPort;
int listenfd, connfd, n;
int fdmax,newfd;
struct sockaddr_in serverInfo,clientInfo;
fd_set master,read_fds;
char socketInputBuffer[10000] = {};
char socketOutputBuffer[10000] = {};

/* user id to ip*/
char userIpList[10][10][50];
int ipListCount = 0;
void userIdToIp(int,int);

int main(int argc , char *argv[])
{	
	if(argc == 2){
		server_init(argv[1]);
	}else{
		printf("Error : Need port parameter like ./server 8888\n");
		return 0;
	}

	int i;

	while(1){
		read_fds = master;
		
		if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
			perror("select");
			return 0;
		}
		for(i = 0; i <= fdmax ; i++){
			if(FD_ISSET(i, &read_fds)){
				if(i == listenfd){
					newClient();
				}else{
					oldClient(i);
				}
			}
		}	
	}
	
	return 0;
}

void server_init(char *port){

	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	serverPort = atoi(port);

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	fdmax = listenfd;
	if(listenfd < 0){
		perror("socket");
		exit(0);
	}

	/* server info */
	bzero(&serverInfo, sizeof(serverInfo));
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = htonl(INADDR_ANY);
	serverInfo.sin_port = htons(serverPort);

	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &serverInfo, sizeof(serverInfo));
	if(bind(listenfd,(struct sockaddr *) &serverInfo, sizeof(serverInfo)) < 0 ){
		perror("bind");
		exit(0);
	}

    if(listen(listenfd, 1024) < 0){
		perror("listen");
		exit(0);
	}

	FD_SET(listenfd, &master);	

	userData = (struct UserData *)malloc(userMAX * sizeof(struct UserData));

	int i;
	for(i = 0 ; i < userMAX ; i++){
		memset(&userData[i],0,sizeof userData[i]);
		userData[i].socket = -1;
		userData[i].step = 0;
	}
}
void newClient(){
	int addrlen = sizeof(clientInfo);
	newfd = accept(listenfd,(struct sockaddr*) &clientInfo, &addrlen);
	if (newfd == -1) {
		perror("accept");
	}else{
		//for new user
		FD_SET(newfd, &master);
		if(newfd > fdmax){
			fdmax = newfd;
		}
		int newIndex = getUserDataMemory(newfd);
		struct in_addr ipAddr = clientInfo.sin_addr;
		inet_ntop(AF_INET, &ipAddr, userData[newIndex].addr, INET_ADDRSTRLEN );
		sprintf(userData[newIndex].port, "%d",clientInfo.sin_port);	

		strcpy(socketOutputBuffer,"****************************************\n");
		strcat(socketOutputBuffer,"** Welcome to the login server. **\n");
		strcat(socketOutputBuffer,"****************************************\n");
		strcat(socketOutputBuffer,"Please input your Account:");
		write(newfd, socketOutputBuffer, strlen(socketOutputBuffer));
	}	
}
void oldClient(int fd){
	char buf[lineMAX];
	char command[100];
	char buf2[100];

	memset(buf,0,sizeof buf);
	int nbytes = read(fd,buf,lineMAX);	
	int index = getUserDataIndexBySocket(fd);
	int i = 0;

	if (nbytes != 0) {
		buf[strcspn(buf, "\n")] = '\0';
	    buf[strcspn(buf, "\r")] = '\0';
		if(userData[index].step == 0){
			strcpy(userData[index].account,buf);
			strcpy(socketOutputBuffer, "Please input your Password:");
			userData[index].step = 1;
			write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
		}else if(userData[index].step == 1){
			strcpy(userData[index].password,buf);
			userData[index].step = 0;
			if(authentication(userData[index].account, userData[index].password,index)){
				//strcpy(socketOutputBuffer, "\nAuthentication success.\n");
				sprintf(socketOutputBuffer, "\nAuthentication success.\n");
				userIdToIp(index, userData[index].id);
				for(i = 0 ; i < userData[index].ipListCount ; i++){
					sprintf(buf2, "%d. %s\n",i,userIpList[userData[index].id][i]);
					strcat(socketOutputBuffer, buf2);					
				}
				strcat(socketOutputBuffer,"Which do you want?  ");		
				//sprintf(command,"./raw %s %d",userData[index].addr,userData[index].id);
				//system(command);
				userData[index].step = 2;
				write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
			}else{
				strcpy(socketOutputBuffer, "\nAuthentication fail\n");
				write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
				closeUser(fd, &master);
			}
			//write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
			//closeUser(fd, &master);
		}else if(userData[index].step == 2){
			int ans = atoi(buf);
			if(ans >= userData[index].ipListCount){
				strcpy(socketOutputBuffer, "Error\n");
			}else if(ans > -1){
				sprintf(socketOutputBuffer, "You select %d. %s\nSuccess.\n", ans,userIpList[userData[index].id][ans]);
				sprintf(command,"./raw %s %d",userIpList[userData[index].id][ans],userData[index].id+1);
				printf("DO %s\n",command);
				system(command);
			}
			write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
            closeUser(fd, &master);
		} 
		//write(fd, socketOutputBuffer, strlen(socketOutputBuffer));
	}
}

int authentication(char *account,char *password,int index){	
	int result = 0;
	FILE *fp;
	fp = fopen(AC, "r");
	if(fp == NULL){
		printf("Error: fopen\n");
		exit(0);
	}
	
    char buf[50];
	char data[2][50];
	
	int i = 0;
    while(fgets(buf,49,fp) != NULL){
		buf[strcspn(buf, "\n")] = '\0';
		strcpy(data[0],buf);
		fgets(buf,512,fp);
		buf[strcspn(buf, "\n")] = '\0';
		strcpy(data[1],buf);
		if((strcmp(data[0],account)) == 0 && (strcmp(data[1],password) == 0)){
			result = 1;
			userData[index].id = i;
		}
		i++;
    }
    fclose(fp);

	return result;
}
void userIdToIp(int index,int id){
    FILE *fp;
    fp = fopen(idtoip, "r");
    if(fp == NULL){
        printf("Error: fopen\n");
        exit(0);
    }

    char buf[50];
	int x,i=0;
    while(fgets(buf,49,fp) != NULL){
        buf[strcspn(buf, "\n")] = '\0';
        x = atoi(buf);
		fgets(buf,512,fp);
        buf[strcspn(buf, "\n")] = '\0';
		if(x == id){
			strcpy(userIpList[id][i],buf);
			i++;
		}
    }
	userData[index].ipListCount = i;
    fclose(fp);

}
