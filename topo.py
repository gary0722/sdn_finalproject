import os
from mininet.topo import *
from mininet.net import *
from mininet.link import *
from mininet.util import *
from mininet.log import *
from mininet.node import *
class MyTopo( Topo ):
	"Simple topology example."

	def __init__( self ):
		"Create custom topo."

		# Initialize topology
		Topo.__init__( self )

		# Add hosts and switches
		core = []
		edgeSwitch = []
		aggSwitch = []
		host = []
		

		s1 = self.addSwitch('s1')
		s2 = self.addSwitch('s2')
		s3 = self.addSwitch('s3')
		h1 = self.addHost('h1')
		h2 = self.addHost('h2')
		h3 = self.addHost('h3')
		h4 = self.addHost('h4')
		h5 = self.addHost('h5')
		h6 = self.addHost('h6')

		self.addLink(s1,s2,bw=100)
		self.addLink(s2,s3,bw=100)
		self.addLink(h1,s1,bw=100)
		self.addLink(h2,s1,bw=100)
		self.addLink(h3,s2,bw=100)
		self.addLink(h4,s2,bw=100)
		self.addLink(h5,s3,bw=100)
		self.addLink(h6,s3,bw=100)

def myTest():
	topo = MyTopo()
	net = Mininet(topo=topo, switch=UserSwitch,link=TCLink,controller=None)
	net.addController('myController',controller=RemoteController,ip='127.0.0.1')
	net.start()
	dumpNodeConnections(net.hosts)
	CLI(net)
	net.stop()

if __name__ == '__main__':
	setLogLevel('info')
	myTest()
#topos = { 'mytopo': ( lambda: MyTopo() ) }
