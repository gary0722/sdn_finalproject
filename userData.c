int getUserDataMemory(int socket){
    int i = 0;
    for(i = 0 ; i < userMAX ; i++){
        if(userData[i].socket == -1){
            userData[i].socket = socket;
            return i;
        }
    }
    return -1;
}
int getUserDataIndexBySocket(int socket){
    int i = 0;
    for(i = 0 ; i < userMAX ; i++){
        if(userData[i].socket == socket){
            return i;
        }
    }
    return -1;
}
void closeUser(int fd, fd_set *master){
    int index = getUserDataIndexBySocket(fd);
    userData[index].socket = -1;
    userData[index].step = 0;
	FD_CLR(fd, master); 
    close(fd);
}

